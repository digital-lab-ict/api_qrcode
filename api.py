import io
import json
import logging
import qrcode
import base64
import StringIO

from flask import Flask, request
from flask.ext.cors import CORS

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
  return "api server v1.0"

qrcode_ecs = {'L': qrcode.constants.ERROR_CORRECT_L,
              'M': qrcode.constants.ERROR_CORRECT_M,
              'Q': qrcode.constants.ERROR_CORRECT_Q,
              'H': qrcode.constants.ERROR_CORRECT_Q}

@app.route("/imaging/1.0/qrcode/encode", methods=['POST'])
def encode():
    logging.info("encode.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        input = request.get_data()
        data = json.loads(input)

        qr = qrcode.QRCode(
            version=data.get("version", 1),
            error_correction=qrcode_ecs.get(data.get("ec", 'M')),
            box_size=data.get("box_size"),
            border=data.get("border")
        )
        qr.add_data(data.get("payload"))
        qr.make(fit=True)

        image = qr.make_image()
	image_buffer = StringIO.StringIO()
	image.save(image_buffer, 'PNG')
        image_b64 = base64.b64encode(image_buffer.getvalue())
        ret_val = {"status": "ok", 'image_b64': image_b64}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/test")
def test():
  return app.send_static_file('test.html')

@app.route("/swagger")
def swagger():
  return app.send_static_file('api_qrcode_swagger.yaml')

if __name__ == "__main__":
  app.run(host='0.0.0.0',port=5000,threaded=True)
